# Adonis NextJS Starter

This is the boilerplate for creating an API server in AdonisJs with Next.js, it comes pre-configured with.

1.  Bodyparser
2.  Authentication
3.  CORS
4.  Lucid ORM
5.  Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --blueprint=omarkhatibco/adonis-nextjs-starter
```

or manually clone the repo and then run `npm install`.

yarn add --dev typescript @types/react @types/node

or 

npm install --save-dev typescript @types/react @types/node

### Migrations

Run the following command to run startup migrations.

```bash
adonis migration:run
```
